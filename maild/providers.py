# -*- coding:utf-8 -*-
"""SMTP providers.
"""
import logging

import jinja2
import voluptuous as schema
import sendgrid
import yaml

LOG = logging.getLogger("maild")

SENDGRID_API_URL = "https://api.sendgrid.com/v3/"

SENDGRID_API_STATUS_OK = 200

EMAIL_SCHEMA = schema.Schema({
    schema.Required("from"): schema.Email(),
    schema.Required("to"): [schema.Email()],
    schema.Optional("cc"): [schema.Email()],
    schema.Optional("bcc"): [schema.Email()],
    schema.Required("subject"): str,
    schema.Required("body"): str
})


class ProviderError(Exception):
    """Occurs when SMTP email delivery failed.
    """

    def __init__(self, msg, data=None):
        super().__init__(msg)
        self.data = data


def _create_log_transport(conf):

    def _send(msg):
        LOG.info("sending email:\n%s", msg)

    return _send


def _create_sendgrid_transport(conf):
    cli = sendgrid.SendGridAPIClient(**conf.sendgrid)

    def _set_recipients(personalizations, msg, recipient_type):
        recipients = msg.get(recipient_type, None)
        if recipients:
            personalizations[recipient_type] = [{
                "email": i
            } for i in recipients]

    def _send(msg):
        personalizations = {"subject": msg.get("subject", None)}
        _set_recipients(personalizations, msg, "to")
        _set_recipients(personalizations, msg, "cc")
        _set_recipients(personalizations, msg, "bcc")
        data = {
            "personalizations":
            personalizations,
            "from": {
                "email": msg["from"]
            },
            "content": [{
                "type": msg.get("content-type", "text/plain"),
                "value": msg["body"]
            }]
        }
        response = cli.client.mail.send.post(request_body=data)
        if response.status_code != SENDGRID_API_STATUS_OK:
            raise ProviderError("SendGrid call failed", {
                "code": response.status_code,
                "body": response.body
            })

    return _send


SMTP_PROVIDERS = {
    "LOG": _create_log_transport,
    "SENDGRID": _create_sendgrid_transport
}


def create_transport(conf):
    """Create SMTP provider.

    Mock this method to create transport for tests.

    Args:
        conf (attrdict.AttrDict): Configuration settings.

    Returns:
        callable(msg: EMAIL_SCHEMA): Method to send emails.

    Raises:
        ValueError: If conf conf.smtp_provider value not in SMTP_PROVIDERS.
    """
    method = SMTP_PROVIDERS.get(conf.smtp_provider, None)
    if method is None:
        raise ValueError("Unknown provider name: %s" % conf.smtp_provider)
    return method(conf)


def create_provider(conf):
    """Create insatnce of email provider.

    Args:
        buildmail (callable): Email builder from templae.
        conf (dict): Provider configuration.
    Returns:
        callable(template_id, **kwargs): Sednmail method.
    """
    loader = jinja2.FileSystemLoader(conf.templates_dir)
    cache = jinja2.Environment(loader=loader)
    transport = create_transport(conf)

    def sendmail(template_id, params):
        try:
            template = cache.get_template("{0}.msg".format(template_id))
            string = template.render(**params)
            raw_msg = yaml.safe_load(string)
            actual_msg = EMAIL_SCHEMA(raw_msg)
            transport(actual_msg)
        except jinja2.exceptions.TemplateNotFound:
            LOG.error("Cannot find template %s", template_id)
            raise ProviderError("Cannot find template", data=template_id)
        except schema.MultipleInvalid as ex:
            data = {
                getattr(e.path[0], "schema", e.path[0]): e.msg
                for e in ex.errors
            }
            LOG.error("Invalid email: %s", data)
            raise ProviderError("Invalid email", data=data)

    return sendmail
