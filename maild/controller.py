# -*- coding:utf-8 -*-
"""Maild service facade.
"""
import asyncio
import logging
import logging.config
import os
import signal

import aiorfc
import voluptuous as schema

from maild import providers
from maild import settings

LOG = logging.getLogger("maild")


def _setup_logging(conf):
    settings.LOG_CFG["handlers"]["file"]["filename"] = os.path.join(
        conf.logging.dir, "maild.log")
    settings.LOG_CFG["loggers"][""]["level"] = conf.logging.level
    logging.config.dictConfig(settings.LOG_CFG)


def _setup_event_loop():
    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGTERM, loop.stop)
    return loop


def onerror(ex):
    """Error handler for maild service.
    """
    if isinstance(ex, providers.ProviderError):
        return True, ex.data
    return False, None


@schema.validate(conf=settings.CONF_SCHEMA)
def start(conf):
    """Start maild server with the configuration provided.

    Args:
        conf(dict): Server configuration.
    """
    _setup_logging(conf)
    loop = _setup_event_loop()
    with aiorfc.RFCConnection(loop, **conf.rpc) as rfc:
        run(conf, rfc, loop)
        loop.run_forever()
    loop.close()


def run(conf, connection, loop):
    """Run maild server on the RFC connection provided.

    This function is public to use tests.

    Args:
        conf (dict): Service configuration.
        connection(aiorfc.RFCConnection): Open RFC connection.
        loop: Event loop.
    """
    handlers = (providers.create_provider(conf), )
    loop.run_until_complete(
        connection.run_server("maild", handlers, onerror=onerror))
