# -*- coding:utf-8 -*-
"""Maild settings definitions.
"""
import cfg.schema

from maild import providers

LOG_LEVELS = {"NOTSET", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"}

LOG_CFG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "standard": {
            "class": "logging.Formatter",
            "format": "%(asctime)s %(name)s [%(levelname)s]: %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "standard",
            "level": "DEBUG"
        },
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "formatter": "standard",
            "level": "DEBUG",
            "filename": "",
            "maxBytes": 1048576,
            "backupCount": 10
        }
    },
    "loggers": {
        "": {
            "handlers": ["file", "console"],
            "level": "",
            "propagate": True
        }
    }
}

LOGGING_SCHEMA = cfg.schema.Schema({
    cfg.schema.Required(
        "dir", ("--log-dir", ),
        default="/tmp",
        description="Log files directory."):
    str,
    cfg.schema.Required(
        "level", ("-l", "--log-level", ),
        default="INFO",
        description="Log level."):
    cfg.schema.In(LOG_LEVELS)
})

RPC_SCHEMA = cfg.schema.Schema({
    cfg.schema.Optional(
        "host",
        cli=("--rpc-host", ),
        default="localhost",
        description="RPC brocker host."):
    cfg.schema.Or(None, str),
    cfg.schema.Optional(
        "port", cli=("--rpc-port", ), description="RPC broker port."):
    cfg.schema.Or(None, cfg.schema.All(int, cfg.schema.Range(min=0,
                                                             max=65535))),
    cfg.schema.Optional(
        "user", cli=("--rcp-user", ), description="RPC broker login user."):
    cfg.schema.Or(None, str),
    cfg.schema.Optional(
        "password",
        cli=("--rpc-password", ),
        description="RPC broker login password."):
    cfg.schema.Or(None, str)
})

SENDGRID_API_SCHEMA = cfg.schema.Schema({
    cfg.schema.Required(
        "host",
        default=providers.SENDGRID_API_URL,
        description="SendGrid API URL."):
    cfg.schema.Url,
    cfg.schema.Required(
        "apikey",
        cli=("--sendgrid-api-key", ),
        description="SendGrid API key."):
    str
})

CONF_SCHEMA = cfg.schema.Schema({
    cfg.schema.Required(
        "templates_dir",
        cli=("-t", "--templates-dir"),
        default="templates",
        description="Email templates location."):
    str,
    cfg.schema.Required(
        "smtp_provider",
        cli=("-s", "--smtp-provider", ),
        default="LOG",
        description="SMTP provider type."):
    cfg.schema.All(cfg.schema.In(providers.SMTP_PROVIDERS)),
    cfg.schema.Optional("rpc", default={}):
    RPC_SCHEMA,
    cfg.schema.Optional("logging", default={"dir": "/tmp", "level": "INFO"}):
    LOGGING_SCHEMA,
    cfg.schema.Optional("sendgrid"):
    SENDGRID_API_SCHEMA
})
