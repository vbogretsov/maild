# -*- coding:utf-8 -*-
import collections
import os

import aiorfc
import attrdict
import pytest

from maild import controller

CONF = attrdict.AttrDict({
    "templates_dir": "/var/lib/maild/templates",
    "logging": {
        "dir": "/tmp",
        "level": "INFO"
    }
})

TEMPLATE_CONFIRM_ACCOUNT = """
from: {{from}}
to: {{to}}
subject: Account confirmation
body: "Hello,
Follow this link to confirm Your account: {{url}}
"
"""

TEMPLATE_RESET_PASSWORD = """
from: {{from}}
to: {{email}}
subject: Password reset
body: "Hello,
Follow this link to reset Your password: {{url}}
"
"""

TEMPLATE_INVALID = """
Body: "Hello!"
"""


@pytest.fixture
def templates(fs):
    fs.CreateFile(
        os.path.join(CONF["templates_dir"], "confirm.msg"),
        contents=TEMPLATE_CONFIRM_ACCOUNT)
    fs.CreateFile(
        os.path.join(CONF["templates_dir"], "pwreset.msg"),
        contents=TEMPLATE_RESET_PASSWORD)
    fs.CreateFile(
        os.path.join(CONF["templates_dir"], "invalid.msg"),
        contents=TEMPLATE_INVALID)


@pytest.fixture
def rfc_connection(request, aioamqpmock, event_loop):
    connection = aiorfc.RFCConnection(event_loop)
    event_loop.run_until_complete(connection.open())

    def fin():
        event_loop.run_until_complete(connection.close())

    request.addfinalizer(fin)
    return connection


@pytest.fixture
def server(event_loop, mocker, rfc_connection, templates):
    inbox = collections.defaultdict(list)

    def transport(msg, **kwargs):
        for address in msg.get("to", ()):
            inbox[address].append(msg)
        for address in msg.get("cc", ()):
            inbox[address].append(msg)
        for address in msg.get("bcc", ()):
            inbox[address].append(msg)

    mocker.patch(
        "maild.providers.create_transport", new=lambda conf: transport)
    log = mocker.patch("maild.providers.LOG")
    controller.run(CONF, rfc_connection, event_loop)
    return attrdict.AttrDict(name="maild", inbox=inbox, log=log)


@pytest.fixture
def client(event_loop, rfc_connection, server):
    return event_loop.run_until_complete(
        rfc_connection.create_client(server.name, timeout=1))


@pytest.mark.asyncio
async def test_sendmail_mail_sent(server, client):
    sender = "test@noreply.com"
    recipient = "user@mail.com"
    params = {
        "from": sender,
        "to": [recipient],
        "url": "https://myservice.org/user/confirm/1234-5678"
    }
    await client.sendmail("confirm", params)
    assert len(server.inbox[recipient]) > 0
    email = server.inbox[recipient][0]
    assert email["to"] == [recipient]
    assert email["from"] == sender
    assert email["subject"] == "Account confirmation"


@pytest.mark.asyncio
async def test_sendmail_logs_error_if_template_not_found(server, client):
    with pytest.raises(aiorfc.RFCCallFailed) as ex:
        await client.sendmail("unknown", {"from": "test@noreply.com"})
    server.log.error.assert_called_with("Cannot find template %s", "unknown")


@pytest.mark.asyncio
async def test_sendmail_logs_error_if_email_invalid(server, client):
    with pytest.raises(aiorfc.RFCCallFailed) as ex:
        await client.sendmail("invalid", {"from": "test@noreply.com"})
    expected_errors = {
        "Body": "extra keys not allowed",
        "to": "required key not provided",
        "body": "required key not provided",
        "subject": "required key not provided",
        "from": "required key not provided"
    }
    server.log.error.assert_called_with("Invalid email: %s", expected_errors)
