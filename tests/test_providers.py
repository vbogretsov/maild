# -*- coding:utf-8 -*-
import attrdict
import pytest

from maild import providers


@pytest.fixture
def test_email():
    return {
        "from": "noreply@domain.com",
        "to": ["user@mail.com"],
        "subject": "Test",
        "body": "Hello!"
    }


@pytest.fixture
def sendgrid_conf():
    return attrdict.AttrDict(
        smtp_provider="SENDGRID", sendgrid={"apikey": None})


@pytest.fixture
def sendgrid_mock(mocker):
    client = mocker.MagicMock()
    mocker.patch("sendgrid.SendGridAPIClient").return_value = client
    return client


def test_create_transport_returns_log_provider_if_smtp_provider_log(
        mocker, test_email):
    log = mocker.patch("maild.providers.LOG.info")
    transport = providers.create_transport(attrdict.AttrDict(
        smtp_provider="LOG"))
    transport(test_email)
    log.assert_called_with("sending email:\n%s", test_email)


def test_create_transport_returns_sg_provider_if_smtp_provider_sg(
        test_email, sendgrid_conf, sendgrid_mock):
    sendgrid_mock.client.mail.send.post.return_value = attrdict.AttrDict(
        status_code=200)
    transport = providers.create_transport(sendgrid_conf)
    transport(test_email)
    expected_data = {
        "personalizations": {
            "to": [{"email": test_email["to"][0]}],
            "subject": test_email["subject"]
        },
        "from": {
            "email": test_email["from"]
        },
        "content": [{
            "type": "text/plain",
            "value": test_email["body"]
        }]
    }
    sendgrid_mock.client.mail.send.post.assert_called_with(
        request_body=expected_data)


def test_sendgrid_raises_provider_error_if_bad_status_code(
        test_email, sendgrid_conf, sendgrid_mock):
    sendgrid_mock.client.mail.send.post.return_value = attrdict.AttrDict(
        status_code=400, body="delivery failed")
    transport = providers.create_transport(sendgrid_conf)
    with pytest.raises(providers.ProviderError):
        transport(test_email)


def test_create_transport_rases_value_error_if_unknown_provider():
    with pytest.raises(ValueError):
        providers.create_transport(attrdict.AttrDict(smtp_provider="UNKNOWN"))
