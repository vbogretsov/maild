import aiorfc
import asyncio


async def test(connection):
    client = await connection.create_client("maild")
    try:
        params = {
            "from": "test@noreply.com",
            "to": ["user@mail.com"],
            "url": "https://myservice.org/user/confirm/1234-5678"
        }
        await client.sendmail("confirmation", params)
    except aiorfc.RFCCallFailed as ex:
        print(ex.ex)


def main():
    loop = asyncio.get_event_loop()
    with aiorfc.RFCConnection(loop) as connection:
        loop.run_until_complete(test(connection))


if __name__ == '__main__':
    main()
