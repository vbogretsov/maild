# maild 0.1.0

Docker container for the AMQP based email delivery service [maild](https://gitlab.com/vbogretsov/maild/) which can send emails either via [SendGrid](https://sendgrid.com) or to a log.
Log based delivery can be usefull during development. The service uses emails prepared as [jinja2](https://pypi.python.org/pypi/Jinja2/2.9.6) templates.

Assuming we have docker container from the official [RabbitMQ image](https://hub.docker.com/_/rabbitmq/) which is connected to the docker network `test`, email templates prepared in folder `/email/templates` and an email template `/email/templates/template1.msg`.

````bash
$ docker create --name maild --network test -e MAILD_RPC_URL=amqp -v /email/templates:/usr/local/maild maild
$ docker start maild
````

Now we can use [aiorfc](https://gitlab.com/vbogretsov/aiorfc/blob/master/README.md) client to send emails.

````python
async def demo(rfc_connection):
    """Demo client for the maild service.

    Args:
        rfc_connection(aiorfc.RFCConnection): Open RPC connection.
    """
    client = await connection.create_client("maild")
    try:
        params = {
            "from": "test@noreply.com",
            "to": ["user@mail.com"]
        }
        await client.sendmail("template1", params)
    except aiorfc.RFCCallFailed as ex:
        print(ex.ex)

````

Container environment variables:

* MAILD_RPC_HOST RPC brocker host.
* MAILD_RPC_PORT RPC broker port.
* MAILD_RPC_USER RPC RPC broker login user.
* MAILD_RPC_PASSWORD RPC broker login password.
* MAILD_LOG_DIR Log files directory.
* MAILD_LOG_LEVEL Log level.
* MAILD_SMTP_PROVIDER SMTP provider type [LOG|SENDGRID].
* MAILD_TEMPLATES_DIR Email templates location.
* MAILD_SENDGRID_API_KEY SendGrid API key.

See maild arguments description in the
[README](https://gitlab.com/vbogretsov/maild/blob/master/README.md) file.

## License

See the [LICENSE](https://gitlab.com/vbogretsov/maild/blob/master/LICENSE) file.