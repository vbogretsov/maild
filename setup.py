# -*- coding:utf-8 -*-
"""A setuptools based setup module for the maild service.
"""

import setuptools


GITLAB_URL = "git+https://gitlab.com/vbogretsov"
DEPENDENCY = "{0}/{1}.git#egg={1}-{2}"

setuptools.setup(
    name="maild",
    version="0.1.0",
    description="AMQP based email service.",
    url="https://gitlab.com/vbogretsov/maild",
    author="vbogretsov",
    author_email="bogrecov@gmail.com",
    license="BSD",
    packages=["maild"],
    py_modules=["mailctl"],
    setup_requires=[
        "pytest-runner"
    ],
    install_requires=[
        "aiorfc",
        "attrdict",
        "cfg",
        "click",
        "jinja2",
        "sendgrid",
        "pyyaml",
    ],
    tests_require=[
        "pytest==3.0.7",
        "pytest-asyncio",
        "pytest-cov",
        "pytest-mock",
        "pytest-aioamqp",
        "pyfakefs"
    ],

    entry_points="""
        [console_scripts]
        maild=mailctl:start
    """,
    dependency_links=[
        DEPENDENCY.format(GITLAB_URL, "aiorfc", "0.2.0"),
        DEPENDENCY.format(GITLAB_URL, "cfg", "0.1.0"),
        DEPENDENCY.format(GITLAB_URL, "pytest-aioamqp", "0.1.0")
    ]
)
