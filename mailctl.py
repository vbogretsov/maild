# -*- coding:utf-8 -*-
"""Service control for maild.
"""
import click
from cfg import config
from cfg import yamlcfg
import setproctitle

from maild import controller
from maild import settings

CONTEXT_SETTINGS = {"help_option_names": ["-h", "--help"]}

CFG_LOADER = config.loader(
    method=yamlcfg.load, default_locations=("/etc/maild.yml", ))


@click.command()
@config.export(settings.CONF_SCHEMA.schema, "maild", loader=CFG_LOADER)
def start(configuration, **kwargs):
    """Start maild service.
    """
    setproctitle.setproctitle("maild")
    controller.start(configuration)
