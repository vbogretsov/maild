FROM python:3.6-alpine

MAINTAINER bogrecov@gmail.com

ENV MAILD_RPC_HOST=localhost
ENV MAILD_LOG_LEVEL=INFO
ENV MAILD_SMTP_PROVIDER=LOG
ENV MAILD_SENDGRID_API_KEY=NONE

COPY ./ /root/

RUN apk update && apk add git gcc musl-dev
RUN adduser -s /bin/nologin -D maild
RUN cd /root && python setup.py install
RUN	cp /root/etc/maild.yml /etc/maild.yml && \
	mkdir -p /var/log/maild && \
	chown -R maild:maild /var/log/maild && \
	mkdir -p /usr/share/maild/ && \
	chown -R maild:maild /usr/share/maild

USER maild

CMD ["maild"]
