# maild 0.1.0

AMQP based email service.

## Installation

### pip way

````bash
$ pip install -e git+https://gitlab.com/vbogretsov/maild#egg=maild-0.1.0
````

### docker way

See the [maild image documentation](https://hub.docker.com/r/vbogretsov/maild/).

## Usage

Start maild service from terminal.

````bash
$ maild [options]
````

Use `--help` to see options description.

Start maild service from python.

````python
from maild import controller

def demo(conf, rfc_connection, event_loop):
    """Start maild service.

    Args:
        conf (dict): Maild service configuration. Should meet the schema
            maild.settings.CONF_SCHEMA.
        rfc_connection (aiorfc.RFCConneciton): Open RPC connection.
        event_loop: asyncio event loop.
    """
    controller.run(conf, rfc_connection, event_loop)
````

Python client.

````python
async def demo(rfc_connection):
    """Demo client for the maild service.

    This example assumes the file template1.msg exists in the templates folder
    of maild service.

    Args:
        rfc_connection(aiorfc.RFCConnection): Open RPC connection.
    """
    client = await connection.create_client("maild")
    try:
        params = {
            "from": "test@noreply.com",
            "to": ["user@mail.com"]
        }
        await client.sendmail("template1", params)
    except aiorfc.RFCCallFailed as ex:
        print(ex.ex)

````

## License

See the [LICENSE](https://gitlab.com/vbogretsov/maild/blob/master/LICENSE) file.