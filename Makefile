PYTHON	=	python3.6
DOCKER	=	docker -H tcp://127.0.0.1:65535
ENV		=	.env
IMG		=	vbogretsov/maild


defaul: test
	


clean:
	@echo Cleaing docker images ...
	$(DOCKER) images | grep none | awk '{print $$3}' | xargs $(DOCKER) rmi
	@echo Cleaing docker images done
	@echo Cleaning build files ...
	rm -rf __pycache__
	rm -rf build
	rm -rf dist
	rm -rf maild.egg-info
	find ./ -name *.pyc | xargs rm
	@echo Cleaning build files done


test:
	@echo Running tests ...
	python setup.py test
	@echo Running tests done


image:
	@echo Building docker image ...
	$(DOCKER) build -t $(IMG) --force-rm .
	@echo Building docker image done


env:
	@echo Creating virtual environment ...
	$(PYTHON) -m venv $(ENV)
	@echo Creating virtual environment done
